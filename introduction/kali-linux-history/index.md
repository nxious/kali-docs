---
title: Kali Linux History
description:
icon:
type: post
weight: 450
author: ["g0tmi1k",]
keywords: [""]
og_description:
---
​
Kali Linux is based on years of knowledge and experience of building a pentestion testing Operating Systems, which has spanned over multiple previous projects.
During all these project's life-time, there has been only a few different developers, as the team has always been small. As a result, Kali has been years in the making and has come a long way.
​

The first project was called **Whoppix**, which stood for **WhiteHat Knoppix**. As can be inferred from the name, it was based on Knoppix for the underlining OS. Whoppix had releases ranging from v2.0 to v2.7.
​

This made way for the next project, **WHAX** _(or the long hand, **WhiteHat Slax**)._ The name change was because the base OS changed from Knoppix to Slax. WHAX started at v3, as a nod towards it carrying on from Whoppix.
​

There was a similar OS being produced at the same time, **Auditor Security Collection** _(often getting shorted to just **Auditor**)_, once again using Knoppix, and efforts were combined (with WHAX) to produce **BackTrack**. BackTrack was based on Slackware from v1 to v3, but switched to Ubutnu later on with v4 to v5.
​

Using the experience gained from all of this, **Kali Linux** came after BackTrack. Kali started off using Debian stable as the engine under the hood before moving to Debian testing when Kali became a rolling OS.
​
- - -
​
Below is a rough overview of how Kali Linux came to be:
​
| Date         | Project Released          | Base OS                  |
|--------------|---------------------------|--------------------------|
| 2004-August  | Whoppix v2                | Knoppix                  |
| 2005-July    | WHAX v3                   | Slax                     |
| 2006-May     | BackTrack v1              | Slackware Live CD 10.2.0 |
| 2007-March   | BackTrack v2              | Slackware Live CD 11.0.0 |
| 2008-June    | BackTrack v3              | Slackware Live CD 12.0.0 |
| 2010-January | BackTrack v4              | Ubuntu 8.10              |
| 2011-May     | BackTrack v5              | Ubuntu 10.04             |
| 2013-March   | Kali Linux v1             | Debian 7                 |
| 2015-August  | Kali Linux v2             | Debian 8                 |
| 2016-January | Kali Linux Rolling        | Debian Testing           |

{{% notice info %}}
This is only the major releases, there were minor ones to address bug fixes, releases, and tools updates.
{{% /notice %}}

For more details about [Kali's history, see this page](https://kali.training/topic/a-bit-of-history/). And for more information about [Kali's releases, see this page](https://www.kali.org/kali-linux-releases/).